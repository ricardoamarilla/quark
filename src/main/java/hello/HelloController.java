package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
       String style = "<style type='text/css' media='screen'>";style += "body { background-color: #6DB23E; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }";         
       //Corporate color: #08273A
       //Test color: LimeGreen
       style += "body { background-color: #6DB23E; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }";
       //style += "body { background-color: #08273A; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }";
        style += "</style>";          
        //TODO - Personalize this message!
        //String message = "The Quark App :: Login";
        String message = "Bienvenido a la aplicacion Quark";
        //String message = "Welcome to the Quark App!";
        //message += "<p><center><img src='https://gitlab.com/the-atomic-lab/quark/raw/master/img/Quark.jpg' alt='Quark' width='250'></center>";
        //message += "<p><center><img src='https://gitlab.com/the-atomic-lab/quark/raw/master/img/Quark_Logo.png' alt='Quark' width='250'></center>";
        //message += "<p><center><img src='https://gitlab.com/the-atomic-lab/quark/raw/master/img/login-form-1.png' alt='Login' width='400'></center>";
        message += "<p><center><img src='https://gitlab.com/the-atomic-lab/quark/raw/master/img/login-form-2.png' alt='Login' width='400'></center>";
        String body = "<body>" + message + "</body>";
        return style + body;
    }

}
