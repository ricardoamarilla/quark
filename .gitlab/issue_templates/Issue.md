# Business Value
Proper messaging and a consistent user experience will _delight_ our Customers and keep them coming back wanting more!

## Tasks
- [ ] Get correct messaging with Product, @ChloeHdz
- [ ] Verify spelling with Tech Writer, @vehernandez
- [ ] User Experience
  - [ ] Get correct Corporate color code for background
  - [ ] Get updated Corporate logo
  - [ ] Write WCAG 2.0 / ADA color tests

### Code Fix Suggestion
```css
background-color: #08273B;
```
```javascript
message = "The Quark App"
```

### Workflow
```mermaid
graph LR;
  hw[Hello World?] --> hw_n{No};
  hw --> hw_y{Yes};
  hw_y --> hw_finish[Greetings!];
  hw_n --> say_hi[Say Hi?];
  say_hi --> say_hi_y{Yes};
  say_hi --> say_hi_n{No};
  say_hi_y --> hello_not_hi[Say 'Hello!']
  say_hi_n --> cheers_mate['Cheers Mate!']
  ```

### Screenshot
This is what currently shows up in `Production`:
