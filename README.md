[![pipeline status](https://gitlab.com/the-atomic-lab/quark/badges/master/pipeline.svg)](https://gitlab.com/the-atomic-lab/quark/commits/master)[![coverage report](https://gitlab.com/the-atomic-lab/quark/badges/master/coverage.svg)](https://gitlab.com/the-atomic-lab/quark/commits/master)[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/42/badge)](https://bestpractices.coreinfrastructure.org/projects/42)

# The Quark App

A **Quark** is

>any of a number of subatomic particles carrying a fractional electric charge, postulated as building blocks of the hadrons. Quarks have not been directly observed but theoretical predictions based on their existence have been confirmed experimentally.
>
>![Quark_sm](img/Quark_sm.jpg)

**The Quark App**, similar to a small *building block* in matter, is a simple application to quickly and easily show the power of GitLab in as little time as possible.

With as broad a feature set found in GitLab, **The Quark App** merely touches on some of the main capabilities; still, this project is meant to give you, the viewer, a quick glimpse of the compelling features in this all-inclusive, single application for the DevOps lifecycle!

## What Does It Do?

From an [Issue](https://docs.gitlab.com/ee/user/project/issues/#use-cases) to a fully-deployed **Dockerized**  application running in a **Kubernetes** cluster [Environment](https://docs.gitlab.com/ce/ci/environments.html), *The Quark App* shows the business value of a Cloud-Native, single platform enabling the entire DevOps lifecycle, from Plan to Monitor.

## The Setup

**The Quark App** uses Docker to build and deploy an application to a Kubernetes cluster.

* This project has a Docker Registry enabled, wherein the images are stored.
* Containers are built, delivered and scaled using a Group-level Kubernetes cluster.
* [GitLab Auto DevOps](https://about.gitlab.com/product/auto-devops/) creates a default **pipeline-as-code** template, which builds, tests, and deploys the application.
* [Prometheus](https://prometheus.io/) provides monitoring, both of GitLab Services, and applications deployed to Kubernetes.

![docker-gitlab-k8s](img/docker-gitlab-k8s.png)

## GitLab Flow

A **Business User** - a Product Owner, Project Manager, Team Lead, or any other type of requestor - creates an **Issue** expressing the business value of the request, whether a *User Story*, *Defect*, *IT Task*, or other item for the **Development Teams**.

A Team Member - a **Developer** - picks up the Issue, and creates a **Merge Request** directly from it.  The automated worfklow creates a corresponding **Branch**, and on first commit, kicks off a CI **Pipeline**.

Results from the **CI Pipeline** populate the Merge Request, to provide the developer and peers - particularly those involved in the **Code Review** - information on the execution of jobs in the pipeline.  Additionally, a **Review App** - an environment built off the branch in progress - is created to provide a live application to further test, review changes, and aid in the validation and approval of the changes in the Merge Request.  This early feedback aids in ensuring the code is:

* Up to standards and conventions
* As free as possible of code security vulnerabilities
* Behaving as expected
* Performing as expected

Once Code Reviews are done, **Approvals** provide a compliant system to sign-off on the changes, and allow for merging the new code back into the *production* branch, i.e., `master`.

Once a the code is merged, the **CD Pipeline** runs, which is deployed to environments such as *Staging* or *Production*. Deployment to these environments can be done either manually or automatically, as defined in the `yml` file.  These environments - including the Review App - can be monitored directly within GitLab.

From **Plan to Monitor**, GitLab provides a single application for the entire DevOps lifecycle.

![Gitlab_Flow](img/Gitlab_Flow.png)